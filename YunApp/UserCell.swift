//
//  UserCell.swift
//  YunApp
//
//  Created by cjnora on 2017/3/26.
//  Copyright © 2017年 YunApp. All rights reserved.
//

import UIKit
import CoreData

class UserCell: UITableViewCell {

    @IBOutlet weak var usrPhoto: UIImageView!
    @IBOutlet weak var usrNameLbl: UILabel!
    @IBOutlet weak var usrMessageLbl: UILabel!
        var coreDataConnect = CoreDataFunc(context: context)

    func updateUI() {
        let selectResult = self.coreDataConnect.retrieve("Group", predicate: nil, sort: [["gid":true],["groupName":true],["member":true]], limit: nil)
        if let results = selectResult {
            for result in results {
                        usrNameLbl.text = "\(result.value(forKey: "member")!)"
                print("\(result.value(forKey: "gid")!).\(result.value(forKey: "groupName")!).\(result.value(forKey: "member")!)")
            }
        }
        usrMessageLbl.text = "test"
        usrPhoto.image = UIImage(named: "u4j")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
