//
//  signupVC.swift
//  YunApp
//
//  Created by cjnora on 2017/3/25.
//  Copyright © 2017年 YunApp. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class signupVC: UIViewController {

    @IBOutlet weak var account: UITextField!
    @IBOutlet weak var password: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func finishBtnPressed(_ sender: Any) {
        if account.text == "" {
            let alertController = UIAlertController(title: "Error", message: "Please enter your email and password", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
            
        } else {
            FIRAuth.auth()?.createUser(withEmail: account.text!, password: password.text!) { (user, error) in
                
                if error == nil {
                    guard let uid = user?.uid else{
                        return
                    }
                    let values = ["email":self.account.text]
                    DataService.instance.usersRef.child(uid).updateChildValues(values as Any as! [AnyHashable : Any], withCompletionBlock:{(err,ref) in
                        if err == nil{
                        print("Saved usrInfo Success")
                        }else{
                            print(err as Any)
                            return
                        }
                    })
                    print("You have successfully signed up")
                    //Goes to the Setup page which lets the user take a photo for their profile picture and also chose a username
                     self.performSegue(withIdentifier: "signupFin", sender: Any?.self)
                    
                } else {
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }

    @IBAction func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
