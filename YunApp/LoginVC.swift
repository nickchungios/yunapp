//
//  LoginVC.swift
//  YunApp
//
//  Created by cjnora on 2017/3/19.
//  Copyright © 2017年 YunApp. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import CoreData

class LoginVC: UIViewController,UITextFieldDelegate {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBOutlet weak var account: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    
    func didTapView(){
        self.view.endEditing(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        account.delegate = self
        password.delegate = self
        let tapRecognizer = UITapGestureRecognizer()
        tapRecognizer.addTarget(self, action: #selector(LoginVC.didTapView))
        self.view.addGestureRecognizer(tapRecognizer)
    }
    override func viewDidAppear(_ animated: Bool) {
//        let loggedin = UserDefaults.standard.string(forKey: "isLoggedin")
//        if loggedin == "true" {
//        performSegue(withIdentifier: "loginSuccess", sender: Any?.self)
//        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if textField == self.account {
            self.password.becomeFirstResponder()
        }
        if textField == self.password {
            self.password.resignFirstResponder()
        }
            return true
    }

    func alertFunc(){
        // create the alert
        let alert = UIAlertController(title: "YunApp", message: "帳號或密碼錯誤", preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func loginBtnPressed(_ sender: Any) {
        if self.account.text == "" || self.password.text == "" {
            
            //Alert to tell the user that there was an error because they didn't fill anything in the textfields because they didn't fill anything in
            
            alertFunc()
    
            
        } else {
            
            FIRAuth.auth()?.signIn(withEmail: self.account.text!, password: self.password.text!) { (user, error) in
                
                if error == nil {
                    if let user = FIRAuth.auth()?.currentUser {
                        if !user.isEmailVerified{
                            let alertVC = UIAlertController(title: "Error", message: "Sorry. Your email address has not yet been verified. Do you want us to send another verification email to \(String(describing: self.account.text)).", preferredStyle: .alert)
                            let alertActionOkay = UIAlertAction(title: "Okay", style: .default) {
                                (_) in
                                user.sendEmailVerification(completion: nil)
                            }
                            let alertActionCancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                            
                            alertVC.addAction(alertActionOkay)
                            alertVC.addAction(alertActionCancel)
                            self.present(alertVC, animated: true, completion: nil)
                        } else {
                            print ("Email verified. Signing in...")
                            UserDefaults.standard.set("true", forKey: "isLoggedin")
                            UserDefaults.standard.synchronize()
                            self.performSegue(withIdentifier: "loginSuccess", sender: Any?.self)
                            
                            let Uid = AuthProvider.Instance.userID()
                            let myEntityName = "UsrInfo"
                            let coreDataConnect = CoreDataFunc(context: context)
                            let seq = 1
                            // insert
                            let insertResult = coreDataConnect.insert(
                                myEntityName, attributeInfo: [
                                    "id" : "\(seq)",
                                    "uid" : Uid,
                                    "email" : self.account.text!
                                ])
                            if insertResult {
                                print("新增資料成功")
                            }
                            
                            AuthProvider.Instance.userName = self.account.text!

                        }
                    }
                    //Print into the console if successfully logged in
//                    print("You have successfully logged in")
//                    
//                    self.performSegue(withIdentifier: "loginSuccess", sender: Any?.self)
//                    
                    
                } else {
                    
                    //Tells the user that there is an error and then gets firebase to tell them the error
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    func alertFuncCS(){
        // create the alert
        let alert = UIAlertController(title: "YunApp", message: "Coming Soon...", preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func signUpBtnPressed(_ sender: Any) {
         self.performSegue(withIdentifier: "signup", sender: Any?.self)
    }

    @IBAction func loginFBbtnPressed(_ sender: Any) {
        alertFuncCS()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
