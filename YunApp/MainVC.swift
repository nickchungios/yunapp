//
//  ViewController.swift
//  YunApp
//
//  Created by cjnora on 2017/3/17.
//  Copyright © 2017年 YunApp. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON

class MainVC: UIViewController {
    
    @IBOutlet weak var timerLbl: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var pmLbl: UILabel!
    @IBOutlet weak var curriculumView: UIView!
    var registerNum:String = ""
    var coreDataConnect = CoreDataFunc(context: context)
    override func viewDidAppear(_ animated: Bool) {
        imageView.startAnimating()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        timerStart()
//        loadJsonData()
        
    }
    
    @IBAction func testBtnPressed(_ sender: Any) {
        curriculumView.isHidden = true
    }
    

    //timer--
    var time = 0;
    var timer = Timer();
    
    func timerStart(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(MainVC.timerAction), userInfo: nil, repeats: true)
    }
    
    func timerReset(){
        time = 0
        timerLbl.text = "0秒前更新"
    }
    
    func timerAction(){
        time += 1
        timerLbl.text = "\(String(time))秒前更新"
    }
    //timer--
    var url1:NSURL!
    var url2:NSURL!
    var url3:NSURL!
    var data1:NSData!
    var data2:NSData!
    var data3:NSData!
    var img11:UIImage!
    var img12:UIImage!
    var img13:UIImage!
    //looadJsonData in coredata func--
    func loadJsonData(){
        print("test")
        var str1:String!
        var str2:String!
        var str3:String!
        let selectResult = self.coreDataConnect.retrieve("Picture", predicate: nil, sort: [["title":true],["url":true]], limit: nil)
        if let results = selectResult {
            for result in results {
                if result.value(forKey: "title") as! String == "MainPic1" {
                    str1 = result.value(forKey: "url") as! String
                    let url = str1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                    self.url1 = NSURL(string: url! as String)
                    self.data1 = NSData(contentsOf: self.url1! as URL)
                    self.img11 = UIImage(data:self.data1! as Data)
                }
                if result.value(forKey: "title") as! String == "MainPic2" {
                    str2 = result.value(forKey: "url") as! String
                    let url = str2.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                    self.url2 = NSURL(string: url! as String)
                    self.data2 = NSData(contentsOf: self.url2! as URL)
                    self.img12 = UIImage(data:self.data2! as Data)
                }
                if result.value(forKey: "title") as! String == "MainPic3" {
                    str3 = result.value(forKey: "url") as! String
                    let url = str3.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                    self.url3 = NSURL(string: url! as String)
                    self.data3 = NSData(contentsOf: self.url3! as URL)
                    self.img13 = UIImage(data:self.data3! as Data)
                }
                print("\(result.value(forKey: "title")!).\(result.value(forKey: "url")!)")
                print("\(str1),\(str2),\(str3)")
            }
        }
        self.imageView.animationImages = [ self.img11!,self.img12!,self.img13!]
        self.imageView.animationDuration = 12
        
        DispatchQueue.global(qos: .userInteractive).async{
            let pmUrl = NSURL(string: "https://www.hhvsia.com/test/")
            let pmJsons = NSData(contentsOf:pmUrl! as URL)
            let pmJson = JSON(pmJsons!)
            let pmStr1:String=pmJson[2].string!
            DispatchQueue.main.async{
                self.pmLbl.text = "PM2.5: \(pmStr1)"
                if(self.registerNum != self.pmLbl.text!) {
                    self.timerReset()
                    self.registerNum = self.pmLbl.text!
                }
            }
        }
    }
    //looadJsonData in coredata func--
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }


}
