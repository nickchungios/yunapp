//
//  CurriculumVC.swift
//  YunApp
//
//  Created by cjnora on 2017/5/4.
//  Copyright © 2017年 YunApp. All rights reserved.
//

import UIKit

class CurriculumVC: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    var currentDate = NSDate()
    
    var calendar = NSCalendar.current
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var contentWidth:CGFloat = 0.0
        
        let scrollWidth = scrollView.frame.size.width
        
        var day = calendar.component(.day, from: currentDate as Date)
        
        var month = calendar.component(.month, from: currentDate as Date)
        
        for x in 0...10{
            
            let dateLbl1 = UILabel()
            let dateLbl2 = UILabel()
            let dateLbl3 = UILabel()
            let dateLbl4 = UILabel()
            let dateLbl5 = UILabel()
            let dateLbl6 = UILabel()
            let dateLbl7 = UILabel()
            
            if (x != 0){
                day = day + 6
            }
            if (month == 1) || (month == 3) || (month == 5) || (month == 7) || (month == 8) || (month == 10) || (month == 12){
                if (day > 31){
                    month = month + 1
                    day = day - 31
                }
            }else{
                if (day > 30){
                    month = month + 1
                    day = day - 30
                }
            }
            
            dateLbl1.text = "\(month)/\(day)"
            
            day = day + 1
            
            if (month == 1) || (month == 3) || (month == 5) || (month == 7) || (month == 8) || (month == 10) || (month == 12){
                if (day > 31){
                    month = month + 1
                    day = day - 31
                }
            }else{
                if (day > 30){
                    month = month + 1
                    day = day - 30
                }
            }
            
            dateLbl2.text = "\(month)/\(day)"
            dateLbl3.text = "\(month)/\(day)"
            dateLbl4.text = "\(month)/\(day)"
            dateLbl5.text = "\(month)/\(day)"
            dateLbl6.text = "\(month)/\(day)"
            dateLbl7.text = "\(month)/\(day)"
        
            var newX:CGFloat = 0.0
            
            newX = scrollWidth * CGFloat(x)
            
            contentWidth += newX
            
            scrollView.addSubview(dateLbl1)
            scrollView.addSubview(dateLbl2)
            scrollView.addSubview(dateLbl3)
            scrollView.addSubview(dateLbl4)
            scrollView.addSubview(dateLbl5)
            scrollView.addSubview(dateLbl6)
            scrollView.addSubview(dateLbl7)
            
            dateLbl1.frame = CGRect(x: newX , y: (scrollView.frame.height + 10), width: 50,height: 25 )
            dateLbl2.frame = CGRect(x: newX + dateLbl1.frame.size.width + 10 , y: (scrollView.frame.height + 10), width: 50,height: 25 )
            let X2 = newX + dateLbl1.frame.size.width + dateLbl2.frame.size.width + 10
            dateLbl3.frame = CGRect(x: X2 + 10 , y: (scrollView.frame.height + 10), width: 50,height: 25 )
            let X3 = X2 + dateLbl3.frame.size.width + 10
            dateLbl4.frame = CGRect(x: X3 , y: (scrollView.frame.height + 10), width: 50,height: 25 )
            let X4 = X3 + dateLbl4.frame.size.width + 10
            dateLbl5.frame = CGRect(x: X4 , y: (scrollView.frame.height + 10), width: 50,height: 25 )
            let X5 = X4 + dateLbl5.frame.size.width + 10
            dateLbl6.frame = CGRect(x: X5 , y: (scrollView.frame.height + 10), width: 50,height: 25 )
            let X6 = X5 + dateLbl6.frame.size.width + 10
            dateLbl7.frame = CGRect(x: X6 , y: (scrollView.frame.height + 10), width: 50,height: 25 )

        }
        scrollView.clipsToBounds = false
        scrollView.contentSize = CGSize(width: contentWidth , height: scrollView.frame.size.height)
    }

}
