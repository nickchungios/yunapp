//
//  User.swift
//  YunApp
//
//  Created by cjnora on 2017/4/7.
//  Copyright © 2017年 YunApp. All rights reserved.
//

import UIKit

struct User {
    private var _email: String
    private var _uid: String
    
    var uid: String {
        return _uid
    }
    
    var email: String {
        return _email
    }
    
    init(uid: String, email: String) {
        _uid = uid
        _email = email
    }
    
    
}
