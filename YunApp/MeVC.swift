//
//  MeVC.swift
//  YunApp
//
//  Created by cjnora on 2017/3/21.
//  Copyright © 2017年 YunApp. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import CoreData
import SwiftyJSON

class MeVC: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    var coreDataConnect = CoreDataFunc(context: context)
    @IBOutlet weak var stdNumber: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("\(AuthProvider.Instance.userName)")
        let coreDataConnect = CoreDataFunc(context: context)
        let selectResult = coreDataConnect.retrieve("UsrInfo", predicate: nil, sort: [["uid":true],["email":true]], limit: nil)
        if let results = selectResult {
            for result in results {
                stdNumber.text = "\(result.value(forKey: "email")!)"
                print("\(result.value(forKey: "uid")!).\(result.value(forKey: "email")!)")
            }
        }
    
        DispatchQueue.global(qos: .userInteractive).async{
//            loadJsonFunc.instance.mainPagePicLoad()
        }
        let predicate = "member = \"\(AuthProvider.Instance.userName)\""
        let deleteResult = coreDataConnect.delete("Group", predicate: predicate)
        if deleteResult {
            print("刪除資料成功")
        }
    }

    

    
    @IBAction func buttonPressed(_ sender: UIButton) {
            // create the alert
            let alert = UIAlertController(title: "YunApp", message: "Coming Soon...", preferredStyle: UIAlertControllerStyle.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func logoutBtnPressed(_ sender: Any) {
        let coreDataConnect = CoreDataFunc(context: context)
        let predicate = "uid = \"\(AuthProvider.Instance.userID())\""
        let deleteResult = coreDataConnect.delete("UsrInfo", predicate: predicate)
        if deleteResult {
            print("刪除資料成功")
        }
        let selectResult = coreDataConnect.retrieve("UsrInfo", predicate: nil, sort: [["uid":true],["email":true]], limit: nil)
        if let results = selectResult {
            for result in results {
                stdNumber.text = "\(result.value(forKey: "email")!)"
                print("\(result.value(forKey: "uid")!).\(result.value(forKey: "email")!)")
            }
        }
        
        if FIRAuth.auth()?.currentUser != nil {
            do {
                try FIRAuth.auth()?.signOut()
                UserDefaults.standard.set("false", forKey: "isLoggedin")
                UserDefaults.standard.synchronize()
               self.performSegue(withIdentifier: "logout", sender: Any?.self)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
