//
//  dormVC.swift
//  YunApp
//
//  Created by cjnora on 2017/3/20.
//  Copyright © 2017年 YunApp. All rights reserved.
//

import UIKit

class dormVC: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func alertFunc(){
        // create the alert
        let alert = UIAlertController(title: "YunApp", message: "Coming Soon...", preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func packageBtnPressed(_ sender: Any) {
        alertFunc()
    }
    @IBAction func dormBtnPressed(_ sender: Any) {
        alertFunc()
    }
    @IBAction func netBtnPressed(_ sender: Any) {
        alertFunc()
    }

    @IBAction func washingMashineBtnPressed(_ sender: Any) {
        alertFunc()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func moreBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
