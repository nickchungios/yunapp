//
//  YunPayVC.swift
//  YunApp
//
//  Created by cjnora on 2017/3/18.
//  Copyright © 2017年 YunApp. All rights reserved.
//

import UIKit
import Foundation

class YunPayVC: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var billBtn: UIButton!
    @IBOutlet weak var balanceBtn: UIButton!
    @IBOutlet weak var trainBtn: UIButton!
    @IBOutlet weak var THSRBtn: UIButton!
    @IBOutlet weak var movieBtn: UIButton!
    @IBOutlet weak var lifePayBtn: UIButton!
    @IBOutlet weak var donateBtn: UIButton!
    @IBOutlet weak var takeAwayBtn: UIButton!
    override func viewDidAppear(_ animated: Bool) {
        imageView.startAnimating()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.animationImages = [ UIImage(named: "example2")!]
        imageView.animationDuration = 12
    }
    func alertFunc(){
        // create the alert
        let alert = UIAlertController(title: "YunApp", message: "Coming Soon...", preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func billBtnPressed(_ sender: Any) {
        alertFunc()
    }
    @IBAction func balaneBtnPressed(_ sender: Any) {
        alertFunc()
    }
    @IBAction func trainBtnPressed(_ sender: Any) {
        alertFunc()
    }
    @IBAction func THSRBtnPressed(_ sender: Any) {
        alertFunc()
    }
    @IBAction func movieBtnPressed(_ sender: Any) {
        alertFunc()
    }
    @IBAction func lifePayBtnPressed(_ sender: Any) {
        alertFunc()
    }
    @IBAction func donateBtnPressed(_ sender: Any) {
        alertFunc()
    }
    @IBAction func takeAwayBtnPressed(_ sender: Any) {
        alertFunc()
    }
    @IBAction func QRcodeBtnPressed(_ sender: Any) {
    }
    @IBAction func payBtnPressed(_ sender: Any) {
    }
    @IBAction func searchBtnPressed(_ sender: Any) {
        alertFunc()
    }
    
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


