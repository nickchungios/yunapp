//
//  Pictures.swift
//  YunApp
//
//  Created by cjnora on 2017/4/27.
//  Copyright © 2017年 YunApp. All rights reserved.
//

import UIKit

struct Pictures {
    private var _title: String
    private var _url: String
    
    var title: String {
        return _title
    }
    
    var url: String {
        return _url
    }
    
    init(title: String, url: String) {
        _title = title
        _url = url
    }
    
    
}
