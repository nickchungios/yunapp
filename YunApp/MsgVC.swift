//
//  MsgVC.swift
//  YunApp
//
//  Created by cjnora on 2017/3/21.
//  Copyright © 2017年 YunApp. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class MsgVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBOutlet weak var tableView: UITableView!
    
    private var users = [User]()
    private var selectedUsers = Dictionary<String, User>()
    
    private var _snapData: Data?
    var coreDataConnect = CoreDataFunc(context: context)
    
    var snapData: Data? {
        set {
            _snapData = newValue
        } get {
            return _snapData
        }
    }

    
    func alertFunc(){
        // create the alert
        let alert = UIAlertController(title: "YunApp", message: "Coming Soon...", preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }


    @IBAction func editBtnPressed(_ sender: Any) {
        alertFunc()
    }
    @IBAction func writeBtnPressed(_ sender: Any) {
        alertFunc()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        checkUserLogin()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsMultipleSelection = false
        DispatchQueue.global(qos: .userInteractive).async{
            
        DataService.instance.usersRef.observeSingleEvent(of: .value) { (snapshot: FIRDataSnapshot) in
            
            if let users = snapshot.value as? Dictionary<String, AnyObject> {
                for (key, value) in users {
                    if let dict = value as? Dictionary<String, AnyObject> {
                        if let email = dict["email"] as? String {
                                let uid = key
                                let user = User(uid: uid, email: email)
                                self.users.append(user)
                                print("\(self.users)")
                            
//                            let insertResult = self.coreDataConnect.insert(
//                                "Group", attributeInfo: [
//                                    "gid" : "\(user.uid)",
//                                    "groupName" : "testgroup",
//                                    "member" : "\(user.email)"
//                                ])
//                            if insertResult {
//                                print("新增資料成功")
//                            }
                        }
                    }
                }
            }
            DispatchQueue.main.async{
//                for usr in self.users {
//                    if usr.uid == AuthProvider.Instance.userID() {
//                        AuthProvider.Instance.userName = usr.email;
//                    }
//                }
            self.tableView.reloadData()
            }
        }
        }


    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell") as! UserCell
        
        cell.updateUI()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = users[indexPath.row]
        selectedUsers[user.uid] = user
        performSegue(withIdentifier: "ChatNow", sender: Any?.self)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let user = users[indexPath.row]
        selectedUsers[user.uid] = nil
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var cnt:Int = 0
        let usrName:String = AuthProvider.Instance.userName
        let selectResult = self.coreDataConnect.retrieve("Group", predicate: nil, sort: [["gid":true],["groupName":true],["member":true]], limit: nil)
        if let results = selectResult {
            for result in results {
                if result.value(forKey: "member") as! String != usrName {
                        cnt += 1
                }

            }
        }
        return cnt
    }
    
    func checkUserLogin(){
        if FIRAuth.auth()?.currentUser?.uid == nil{
            perform(#selector(logout), with: nil, afterDelay: 0)
        }
    }
    func logout(){
        if FIRAuth.auth()?.currentUser != nil {
            do {
                try FIRAuth.auth()?.signOut()
                self.performSegue(withIdentifier: "logout", sender: Any?.self)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
