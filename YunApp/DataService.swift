//
//  DataService.swift
//  YunApp
//
//  Created by cjnora on 2017/3/26.
//  Copyright © 2017年 YunApp. All rights reserved.
//


let FIR_CHILD_USERS = "usrInfo"

import Foundation
import FirebaseDatabase
import FirebaseStorage

class DataService {
    private static let _instance = DataService()
    
    static var instance: DataService {
        return _instance
    }
    
    var mainRef: FIRDatabaseReference {
        return FIRDatabase.database().reference()
    }
    
    var usersRef: FIRDatabaseReference {
        return mainRef.child(FIR_CHILD_USERS)
    }
    var messagesRef: FIRDatabaseReference {
        return mainRef.child(Constants.MESSAGES);
    }
    var mediaMessagesRef: FIRDatabaseReference {
        return mainRef.child(Constants.MEDIA_MESSAGES);
    }
    
    var mainStorageRef: FIRStorageReference {
        return FIRStorage.storage().reference(forURL: "gs://yunapp-194c3.appspot.com")
    }
    
    var imagesStorageRef: FIRStorageReference {
        return mainStorageRef.child("images")
    }
    
    var videoStorageRef: FIRStorageReference {
        return mainStorageRef.child("videos")
    }
    
    func saveUser(uid: String) {
        let profile: Dictionary<String, AnyObject> = ["firstName": "" as AnyObject, "lastName": "" as AnyObject]
        mainRef.child(FIR_CHILD_USERS).child(uid).child("profile").setValue(profile)
    }
    
    
    
    
}
