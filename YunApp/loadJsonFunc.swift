//
//  loadJsonFunc.swift
//  YunApp
//
//  Created by cjnora on 2017/4/27.
//  Copyright © 2017年 YunApp. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

class loadJsonFunc{
    
    private static let _instance = loadJsonFunc()
    
    static var instance: loadJsonFunc {
        return _instance
    }
    
    func mainPagePicLoad(){
        let coreDataConnect = CoreDataFunc(context: context)
        let url = NSURL(string: "https://www.hhvsia.com/yuntech_splash2.json")
        let jsons = NSData(contentsOf:url! as URL)
        let json = JSON(jsons!)
        let isEmpty = coreDataConnect.entityIsEmpty("Picture")
        if isEmpty {
            // insert
            let insertResult = coreDataConnect.insert(
                "Picture", attributeInfo: [
                    "title" : "MainPic1",
                    "url" : "\(json[0].string!)"
                ])
            if insertResult {
                print("新增資料1成功")
            }
            let insertResult2 = coreDataConnect.insert(
                "Picture", attributeInfo: [
                    "title" : "MainPic2",
                    "url" : "\(json[1].string!)"
                ])
            if insertResult2 {
                print("新增資料2成功")
            }
            let insertResult3 = coreDataConnect.insert(
                "Picture", attributeInfo: [
                    "title" : "MainPic3",
                    "url" : "\(json[2].string!)"
                ])
            if insertResult3 {
                print("新增資料3成功")
            }
        }else{
        // update
        let predicate1 = "title = \"MainPic1\""
        let updateResult1 = coreDataConnect.update("Picture", predicate: predicate1, attributeInfo: ["url":"\(json[0].string!)"])
        if updateResult1 {
            print("更新資料成功1")
        }
        let predicate2 = "title = \"MainPic2\""
        let updateResult2 = coreDataConnect.update("Picture", predicate: predicate2, attributeInfo: ["url":"\(json[1].string!)"])
        if updateResult2 {
            print("更新資料成功2")
        }
        let predicate3 = "title = \"MainPic3\""
        let updateResult3 = coreDataConnect.update("Picture", predicate: predicate3, attributeInfo: ["url":"\(json[2].string!)"])
        if updateResult3 {
            print("更新資料成功3")
        }
        let selectResult = coreDataConnect.retrieve("Picture", predicate: nil, sort: [["title":true],["url":true]], limit: nil)
        if let results = selectResult {
            for result in results {
                print("\(result.value(forKey: "title")!).\(result.value(forKey: "url")!)")
            }

    }
        }
    }
    func msgListLoad(){
        
    
    }

}
