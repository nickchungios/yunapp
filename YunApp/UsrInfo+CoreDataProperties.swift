//
//  UsrInfo+CoreDataProperties.swift
//  YunApp
//
//  Created by cjnora on 2017/4/22.
//  Copyright © 2017年 YunApp. All rights reserved.
//

import Foundation
import CoreData


extension UsrInfo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UsrInfo> {
        return NSFetchRequest<UsrInfo>(entityName: "UsrInfo")
    }

    @NSManaged public var uid: String?
    @NSManaged public var email: String?
    @NSManaged public var id: Int32

}
