//
//  QRcodeGenerator.swift
//  YunApp
//
//  Created by cjnora on 2017/5/16.
//  Copyright © 2017年 YunApp. All rights reserved.
//

import UIKit

class QRcodeGenerator: UIViewController {

    @IBOutlet weak var input: UITextField!
    @IBOutlet weak var generateBtn: UIButton!
    @IBOutlet weak var QRcodeImg: UIImageView!
    @IBOutlet weak var slider: UISlider!
    
    var qrcodeImg: CIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    // MARK: IBAction method implementation

    @IBAction func generateBtnPressed(_ sender: Any) {
        if qrcodeImg == nil {
            if input.text == "" {
                return
            }
            
            let data = input.text!.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
            
            let filter = CIFilter(name: "CIQRCodeGenerator")
            
            filter?.setValue(data, forKey: "inputMessage")
            filter?.setValue("Q", forKey: "inputCorrectionLevel")
            
            qrcodeImg = filter!.outputImage
            
            input.resignFirstResponder()
            
            generateBtn.setTitle("Clear", for: UIControlState())
            
            displayQRCodeImage()
        }
        else {
            QRcodeImg.image = nil
            qrcodeImg = nil
            generateBtn.setTitle("Generate", for: UIControlState())
        }
        
//        generateBtn.isEnabled = !generateBtn.isEnabled
//        slider.isHidden = !slider.isHidden
    }
    
    // MARK: Custom method implementation
    
    func displayQRCodeImage() {
        let scaleX = QRcodeImg.frame.size.width / qrcodeImg.extent.width
        let scaleY = QRcodeImg.frame.size.height / qrcodeImg.extent.height
        
        let transformedImage = qrcodeImg.applying(CGAffineTransform(scaleX: scaleX, y: scaleY))
        
        QRcodeImg.image = UIImage.init(ciImage: transformedImage)
    }

    
    @IBAction func sliderMoved(_ sender: Any) {
        QRcodeImg.transform = CGAffineTransform(scaleX: CGFloat(slider.value), y: CGFloat(slider.value))
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}
